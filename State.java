package machine;

public enum State {
    GET_ACTION, TAKE_PROFIT, GET_COFFEE_ART,
    FILL_WATER, FILL_MILK, FILL_BEANS, FILL_CUPS
}
