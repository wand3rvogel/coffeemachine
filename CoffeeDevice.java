package machine;

public class CoffeeDevice {

    private int waterTank;
    private int milkTank;
    private int beansTank;
    private int disposableCups;
    private int money;
    private final CoffeeDrink[] coffeeDrinks;
    private State currentState;

    public CoffeeDevice(int waterTank, int milkTank, int beansTank, int disposableCups, int money) {
        this.waterTank = waterTank;
        this.milkTank = milkTank;
        this.beansTank = beansTank;
        this.disposableCups = disposableCups;
        this.money = money;

        coffeeDrinks = new CoffeeDrink[]{
                new CoffeeDrink("espresso", 250, 0, 16, 4),
                new CoffeeDrink("latte", 350, 75, 20, 7),
                new CoffeeDrink("cappuccino", 200, 100, 12, 6)
        };

        setState(State.GET_ACTION);
    }

    public void process(String input) {

        switch (currentState) {
            case GET_ACTION:
                processAction(input);
                break;
            case GET_COFFEE_ART:
                sell(input);
                setState(State.GET_ACTION);
                break;
            case FILL_WATER:
                fill("water", input);
                break;
            case FILL_MILK:
                fill("milk", input);
                break;
            case FILL_BEANS:
                fill("beans", input);
                break;
            case FILL_CUPS:
                fill("cups", input);
                break;
        }
    }

    private void processAction(String action) {

        switch (action) {
            case "buy":
                setState(State.GET_COFFEE_ART);
                break;
            case "fill":
                setState(State.FILL_WATER);
                break;
            case "take":
                takeProfit();
                setState(State.GET_ACTION);
                break;
            case "remaining":
                printContent();
                setState(State.GET_ACTION);
                break;
            case "exit":
                System.exit(0);
                break;
        }
    }

    private void setState(State newState) {

        switch (newState) {

            case GET_ACTION:
                System.out.println("\nWrite action (buy, fill, take, remaining, exit):");
                currentState = State.GET_ACTION;
                break;

            case GET_COFFEE_ART:
                System.out.println("\nWhat do you want to buy? " +
                        "1 - espresso, 2 - latte, 3 - cappuccino" +
                        ", back - to main menu:");
                currentState = State.GET_COFFEE_ART;
                break;

            case FILL_WATER:
                System.out.println("Write how many ml of water you want to add:");
                currentState = State.FILL_WATER;
                break;

            case FILL_MILK:
                System.out.println("Write how many ml of milk you want to add:");
                currentState = State.FILL_MILK;
                break;

            case FILL_BEANS:
                System.out.println("Write how many grams of coffee beans you want to add:");
                currentState = State.FILL_BEANS;
                break;

            case FILL_CUPS:
                System.out.println("Write how many disposable cups of coffee you want to add:");
                currentState = State.FILL_CUPS;
                break;
        }
    }

    private void takeProfit() {

        System.out.printf("I gave you $%d%n", money);
        money = 0;
    }

    private void fill(String ingredient, String strCount) {

        int count = Integer.parseInt(strCount);
        switch (ingredient) {
            case "water":
                waterTank += count;
                setState(State.FILL_MILK);
                break;
            case "milk":
                milkTank += count;
                setState(State.FILL_BEANS);
                break;
            case "beans":
                beansTank += count;
                setState(State.FILL_CUPS);
                break;
            case "cups":
                disposableCups += count;
                setState(State.GET_ACTION);
                break;
        }
    }

    private void makeCoffee(int drinkNum) {

        waterTank -= coffeeDrinks[drinkNum - 1].getWater();
        milkTank -= coffeeDrinks[drinkNum - 1].getMilk();
        beansTank -= coffeeDrinks[drinkNum - 1].getBeans();
        disposableCups--;
        money += coffeeDrinks[drinkNum - 1].getPrice();
    }

    private void sell(String choice) {

        if ("back".equals(choice)) {
            return;
        }
        int drink = Integer.parseInt(choice);
        if (isEnoughResourcesFor(drink)) {
            System.out.println("I have enough resources, making you a coffee!");
            makeCoffee(drink);
        }
    }

    private void printContent() {
        System.out.println("\nThe coffee machine has:");
        System.out.printf("%d ml of water%n", waterTank);
        System.out.printf("%d ml of milk%n", milkTank);
        System.out.printf("%d g of coffee beans%n", beansTank);
        System.out.printf("%d disposable cups%n", disposableCups);
        System.out.printf("$%d of money%n", money);
    }


    private boolean isEnoughResourcesFor(int drinkNum) {

        if (waterTank < coffeeDrinks[drinkNum - 1].getWater()) {
            System.out.println("Sorry, not enough water!");
            return false;
        }
        if (milkTank < coffeeDrinks[drinkNum - 1].getMilk()) {
            System.out.println("Sorry, not enough milk!");
            return false;
        }
        // ...
        return true;
    }

}
