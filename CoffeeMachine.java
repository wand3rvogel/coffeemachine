package machine;

import java.util.Scanner;

public class CoffeeMachine {
    
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        CoffeeDevice machine = new CoffeeDevice(400, 540, 120, 9, 550);

        String input;
        while (true) {
            input = scanner.nextLine();
            machine.process(input);
        }
    }
}
